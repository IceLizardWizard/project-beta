import { NavLink } from 'react-router-dom';
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Customer Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Salesperson Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/history">Salesperson History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">Sales Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new/">Manufacturer Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new/">Vehicle Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new/">Automobile Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/Service">Service</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/Add_Service">Add Service</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="technicians">List Technician</NavLink>
          </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/create">Add Technician</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="technicians">List Technician</NavLink>
          </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/create">Add Technician</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
export default Nav;
