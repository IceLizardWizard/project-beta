import { useState, useEffect } from "react";

export default function AppoinmentList() {
	const [serviceAppointments, setServiceAppointments] = useState([]);
	const [automobiles, setAutomobiles] = useState([]);

	const getData = async () => {
		const url = "http://localhost:8080/api/appointments/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setServiceAppointments(data.appointments);
		}

		const url2 = "http://localhost:8100/api/automobiles/";
		const response2 = await fetch(url2);

		if (response2.ok) {
			const data2 = await response2.json();
			setAutomobiles(data2.autos);
		}
	};

	useEffect(() => {
		getData();
	}, []);

	const handleFinishAppointment = async (id) => {
		const url = `http://localhost:8080/api/appointments/${id}/finish/`;
		const response = await fetch(url, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			getData();
		}
	};

	const handleCancelAppointment = async (id) => {
		const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const response = await fetch(url, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			getData();
		}
	};

	const formatDateTime = (isoString) => {
		const dateTime = new Date(isoString);
		const date = dateTime.toLocaleDateString();
		const time = dateTime.toLocaleTimeString();
		return { date, time };
	};

	const filteredAppointments = serviceAppointments.filter(
		(appointment) =>
			appointment.status !== "Completed" &&
			appointment.status !== "Cancelled"
	);

	return (
		<>
			<h1>Service Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>is VIP?</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{filteredAppointments.map((appointment) => {
						const technicianFullName = `${appointment.technician.first_name} ${appointment.technician.last_name}`;

						const { date, time } = formatDateTime(
							appointment.date_time
						);

						const automobile = automobiles.find(
							(auto) => auto.vin === appointment.vin
						);

						return (
							<tr key={appointment.id} value={appointment.id}>
								<td>{appointment.vin}</td>
								<td>
									{automobile
										? automobile.sold
											? "Yes"
											: "No"
										: "No"}
								</td>
								<td>{appointment.customer}</td>
								<td>{date}</td>
								<td>{time}</td>
								<td>{technicianFullName}</td>
								<td>{appointment.reason}</td>
								<td>{appointment.status}</td>
								<td>
									<button
										className="btn btn-success"
										onClick={() =>
											handleFinishAppointment(
												appointment.id
											)
										}
									>
										Finish
									</button>
									<button
										className="btn btn-danger"
										onClick={() =>
											handleCancelAppointment(
												appointment.id
											)
										}
									>
										Cancel
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

