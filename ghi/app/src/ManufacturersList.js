import {useState, useEffect} from 'react';


function ManufacturerList() {
    const [mans, setMan] = useState([]);


    useEffect(() => {
       async function loadManufacurers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
          const data = await response.json();

            setMan(data.manufacturers);
            //setCustomers(data.customer) i believe this is the name of the field not the url.
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadManufacurers();
    },[])



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer name</th>
        </tr>
      </thead>
      <tbody>
        {mans.map(manufacturer => {
          return (
            <tr key={manufacturer.id} >
              <td>{ manufacturer.name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
export default ManufacturerList;