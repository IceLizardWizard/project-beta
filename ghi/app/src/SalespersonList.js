import {useState, useEffect} from 'react';

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([])


    useEffect(() => {
       async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
          const data = await response.json();

            setSalespeople(data.salesperson)
            //setCustomers(data.customer) i believe this is the name of the field not the url.
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadSalespeople();
    },[])



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First name</th>
          <th>Last name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map(salesperson => {
          return (
            <tr key={salesperson.id} >
              <td>{ salesperson.first_name }</td>
              <td>{ salesperson.last_name }</td>
              <td>{ salesperson.employee_id }</td>
              {/* <td><button onClick={() => handleDelete(hat.id)} className="btn btn-primary" >Delete</button></td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
  export default SalespersonList;