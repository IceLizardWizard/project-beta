import React, { useState, useEffect } from 'react';

function VehicleModelForm( { onFormSubmit }) {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const fetchManufacturers = async () => {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      const data = await response.json();
      setManufacturers(data.manufacturers);
    };

    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/models/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        ...formData,
        manufacturer_id: Number(formData.manufacturer_id),
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      credentials: 'include',
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });
      if (onFormSubmit) {
        onFormSubmit();
      }
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-vehicleModel-form">
            <div className="form-floating mb-3">
                <input required type="text" value={formData.name} onChange={handleFormChange}  placeholder="Name" name="name" id="name" className="form-control" />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="form-floating mb-3">
              <select onChange={handleFormChange} value={formData.manufacturer_id} placeholder="manufacturer_id" required name="manufacturer_id" id="manufacturer_id" className="form-control">
                <option value="">Select a manufacturer</option>
                {manufacturers.map(man => (
                  <option key={man.id} value={man.id}>{man.name}</option>
                ))}
              </select>
              <label htmlFor="manufacturer_id">Manufacturer</label>
              </div>
            <button type="submit">Create</button>
          </form>
          </div>
      </div>
    </div>
  );
}

export default VehicleModelForm;