import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');

  useEffect(() => {
    async function loadSales() {

        const response = await fetch('http://localhost:8090/api/sales/');

        const data = await response.json();
        setSales(data.sales);
    }

    loadSales();
  }, []);

  useEffect(() => {
    // Filter sales based on the selected salesperson
    const filtered = sales.filter((sale) =>
      selectedSalesperson ? sale.salesperson.last_name === selectedSalesperson : true
    );

    setFilteredSales(filtered);
  }, [sales, selectedSalesperson]);

  return (
    <div>
      <label>
        Filter by Salesperson:
        <select
          value={selectedSalesperson}
          onChange={(e) => setSelectedSalesperson(e.target.value)}
        >
          <option value="">All Salespersons</option>
          {Array.from(new Set(sales.map((sale) => sale.salesperson.last_name))).map(
            (lastName) => (
              <option key={lastName} value={lastName}>
                {lastName}
              </option>
            )
          )}
        </select>
      </label>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Salesperson Last Name</th>
            <th>Customer Last Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.automobile.vin}</td>
              <td>{sale.salesperson.last_name}</td>
              <td>{sale.customer.last_name}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonHistory;