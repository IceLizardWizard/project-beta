import {useState, useEffect} from 'react';

function VehicleModelList() {
    const [vms, setVM] = useState([])


    useEffect(() => {
       async function loadVehicleModel() {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
          const data = await response.json();

            setVM(data.models)
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadVehicleModel();
    },[])



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Picture </th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {vms.map(model => {
          return (
            <tr key={model.id} >
              <td>{ model.name }</td>
              <td>
                <img src={ model.picture_url } alt="car" />
                </td>
              <td>{ model.manufacturer.name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
  export default VehicleModelList;
// my brain melted hours ago