import React, {useState, useEffect} from 'react';

function SalesForm () {
  const [automobiles, setAutomobiles] = useState([])
  const [salespeoples, setSalespeople] = useState([])
  const [customers, setCustomers] = useState([])
  const [formData, setFormData] = useState({

    automobiles:'',
    salesperson:'',
    customer:'',
    price: ''
  })


  const getData = async () => {
    const autourl = 'http://localhost:8100/api/automobiles/';
    const salespersonurl = 'http://localhost:8090/api/salespeople/';
    const customersurl = 'http://localhost:8090/api/customers/';
    const responses = await Promise.all(
        [fetch(autourl),
        fetch(salespersonurl),
        fetch(customersurl),]
    );

    const data = await Promise.all(responses.map((res) => res.json()));


    if (responses.every((res) => res.ok)) {
      setAutomobiles(data[0].autos);

      setSalespeople(data[1].salesperson);

      setCustomers(data[2].customer);

    }
  }


  useEffect(()=> {
    getData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const SalesUrl = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(SalesUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        automobiles:'',
        salesperson:'',
        customer:'',
        price: ''
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData((formData) => ({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    }));
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new sale</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="mb-3">
                <select value={formData.automobiles} onChange={handleFormChange} required name="automobiles" id="automobiles" className="form-select">
                    <option value="">Choose an automobile</option>
                    {automobiles.map((auto) => (
                        <option key={auto.id} value={auto.vin}>
                            {auto.vin}
                        </option>
                    ))}
                    </select>
            </div>
            <div className="mb-3">
                <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salespersonSelect" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeoples.map(salespersons => (
                        <option key={salespersons.id} value={salespersons.last_name}>
                            {salespersons.last_name}
                        </option>
                    ))}
                </select>
            </div>
            <div className="mb-3">
                <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customerSelect" className="form-select">
                    <option value="">Choose a customer</option>
                    {customers.map(customer => (
                        <option key={customer.id} value={customer.last_name}>
                            {customer.last_name}
                        </option>
                    ))}
                </select>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.price} onChange={handleFormChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;