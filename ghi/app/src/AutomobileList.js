import {useState, useEffect} from 'react';

function AutomobileList() {
    const [autos, setAuto] = useState([]);


    useEffect(() => {
       async function loadAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
          const data = await response.json();

            setAuto(data.autos);
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadAutomobiles();
    },[]);



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Color</th>
          <th>Year</th>
          <th>Vin</th>
          <th>Model</th>
        </tr>
      </thead>
      <tbody>
        {autos.map(auto => {
          return (
            <tr key={auto.id} >
              <td>{ auto.color }</td>
              <td>{ auto.year }</td>
              <td>{ auto.vin }</td>
              <td>{ auto.model.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
  export default AutomobileList;