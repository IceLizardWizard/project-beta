import {useState, useEffect} from 'react';

function SalesList() {
    const [sales, setSales] = useState([])


    useEffect(() => {
       async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
          const data = await response.json();

            setSales(data.sales)
            //setCustomers(data.customer) i believe this is the name of the field not the url.
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadSales();
    },[])



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>Salesperson Last Name </th>
          <th>Customer Last Name</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map(sale => {
          return (
            <tr key={sale.id} >
              <td>{ sale.automobile.vin }</td>
              <td>{ sale.salesperson.last_name }</td>
              <td>{ sale.customer.last_name }</td>
              <td>{ sale.price }</td>
              {/* <td><button onClick={() => handleDelete(hat.id)} className="btn btn-primary" >Delete</button></td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
  export default SalesList;