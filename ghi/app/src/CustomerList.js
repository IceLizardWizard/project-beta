import {useState, useEffect} from 'react';

function CustomerList() {
    const [customers, setCustomers] = useState([]);


    useEffect(() => {
       async function loadCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok) {
          const data = await response.json();

            setCustomers(data.customer);
        } else {
          console.error('Failed to fetch:', response.statusText);
        }
        }
      loadCustomers();
    },[])



return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First name</th>
          <th>Last name</th>
          <th>Address</th>
          <th>Phone Number</th>
        </tr>
      </thead>
      <tbody>
        {customers.map(customer => {
          return (
            <tr key={customer.id} >
              <td>{ customer.first_name }</td>
              <td>{ customer.last_name }</td>
              <td>{ customer.address }</td>
              <td>{ customer.phone_number }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}
  export default CustomerList;