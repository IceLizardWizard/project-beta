import React, { useState, useEffect } from 'react';



// const [bins, setBins] = useState([]); Two shot reference
function TechnicianForm() {
    const [formData, setFormData] = useState({
      first_name: '',
      last_name: '',
      employee_id: '',
    });

    const onFormSubmit = () => {
      console.log('Technician added!');

    };

    const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8080/api/technicians/';

      const fetchConfig = {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      };

      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        setFormData({
          first_name: '',
          last_name: '',
          employee_id: '',
        });
        onFormSubmit();
      }
    };

    const handleFormChange = (e) => {
      const value = e.target.value;
      const inputName = e.target.name;
      setFormData({
        formData,
        [inputName]: value,
      });
    };

    return (
      <form onSubmit={handleSubmit}>
        <label>
          First Name:
          <input type="text" name="first_name" value={formData.first_name} onChange={handleFormChange} />
        </label>
        <label>
          Last Name:
          <input type="text" name="last_name" value={formData.last_name} onChange={handleFormChange} />
        </label>
        <label>
          Employee ID:
          <input type="text" name="employee_id" value={formData.employee_id} onChange={handleFormChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }

  export default TechnicianForm;