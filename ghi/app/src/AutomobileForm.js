import React, { useState, useEffect } from 'react';

function AutomobileForm({ onFormSubmit }) {
  const [formData, setFormData] = useState({
    year: '',
    color: '',
    vin: '',
    model_id: '',
  });

  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchModels = async () => {
      const response = await fetch('http://localhost:8100/api/models/');
      const data = await response.json();
      setModels(data.models);
    };

    fetchModels();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/automobiles/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        ...formData,
        model_id: Number(formData.model_id),
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      credentials: 'include',
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        year: '',
        color: '',
        vin: '',
        model_id: '',
      });
      if (onFormSubmit) {
        onFormSubmit();
      }
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create New Automobile</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.year} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
              <label htmlFor="year">year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">vin</label>
            </div>
            <div className="form-floating mb-3">
              <select onChange={handleFormChange} value={formData.model_id} placeholder="Model ID" required name="model_id" id="modelId" className="form-control">
                <option value="">select model</option>
                {models.map(model => (
                  <option key={model.id} value={model.id}>{model.name}</option>
                ))}
              </select>
              <label htmlFor="modelId">Model ID</label>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;