import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileForm from './AutomobileForm';
import VehicleModelForm from './VehichleModelForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespersonList from './SalespersonList';
import SalesList from './SalesList';
import ManufacturerList from './ManufacturersList';
import VehicleModelList from './VehicleModelList';
import AutomobileList from './AutomobileList';
import SalespersonForm from './SalespersonForm';
import SalesForm from './SalesForm';
import SalespersonHistory from './SalespersonHistory'
import AppointmentsByVin from './AppointmentsByVin';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="history" element={<SalespersonHistory />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path= "technicians" element = {<TechnicianList />} />
          <Route path="technicians/create" element={<TechnicianForm />} />
          <Route path="manufacturers/new/" element={<ManufacturerForm />} />
          <Route path="automobiles/new/" element={<AutomobileForm />} />
          <Route path="Vehicle/new/" element={<VehicleModelForm />} />
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="vin" element={<AppointmentsByVin />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
