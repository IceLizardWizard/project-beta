import React from "react";
import {useState, useEffect } from "react";



  function TechnicianList() {
  const [technicians, setTechnicians] = useState ([])
  const getData = async () => {
    const url = 'http://localhost:8080/api/technicians';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
      console.log(data)
    }
    }
    useEffect(()=>{
      getData()
}, [])
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians?.map((technician) => (
              <tr key={technician.id} value={technician.id} >
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
                <td>{ technician.employee_id }</td>
              </tr>

          ))}
        </tbody>
      </table>
      </div>
  )

  }

export default TechnicianList;