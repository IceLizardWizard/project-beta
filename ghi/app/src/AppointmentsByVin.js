import React, { useState } from 'react';

function AppointmentsByVin() {
  const [vin, setVin] = useState('');
  const [appointments, setAppointments] = useState([]);
  const [error, setError] = useState(null);

  const fetchAppointments = async (vin) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments?vin=${vin}`);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setAppointments(data.appointments);
    } catch (error) {
      setError(error.message);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    fetchAppointments(event.target.elements[0].value);
  };

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>
          VIN:
          <input type="text" value={vin} onChange={e => setVin(e.target.value)} />
        </label>
        <input type="submit" value="Submit" />
      </form>
      <h1>Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Reason</th>
            <th>Status</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Technician</th>
            <th>Date and Time</th>
            <th>VIP Status</th>

          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.date_time}>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{new Date(appointment.date_time).toLocaleString()}</td>
              <td>
            </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentsByVin;
