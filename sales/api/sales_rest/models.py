from django.db import models




# Create your models here.

class AutomobileVO(models.Model):
#An AutomobileVO model containing vin and sold fields.
    vin = models.CharField(max_length=17, primary_key=True)
    sold = models.BooleanField(default=False)



class Salesperson(models.Model):
#A Salesperson model containing first_name, last_name, employee_id fields.
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        name = self.first_name + " " + self.last_name
        return name

class Customer(models.Model):
# A Customer model containing first_name, last_name, address, and phone_number fields.
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=400)
    phone_number = models.CharField(max_length=16)

    def __str__(self):
        name = self.first_name + " " + self.last_name
        return name

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE
    )
    price = models.FloatField()