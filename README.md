# CarCar

CarCar is an application designed to offer functionality that a dealership might impliment. It is an inventory, automobile sales, and automobile service management system.

Quick note from the dev team:

We hope you enjoy our application. It wasn't an easy journey but we got here. Some functionality has been sacrificed due to time restraints. These will be explained later.

Team:

* Chase Fienhold - Sales
* Judah Viggers - Services

## Design

## Service microservice

The services is made up of:

    Technician:
        It is independent from any info in inventory.

        Technician has a model in service_rest/models.py and it has a view that "GET", "DELETE", "PUT", and "POST" in service/service_rest/views.py

    Appointment:
        Appointment is dependent on the information from inventory specifically automobiles' vin and services technician.

        Appointment has a model in service_rest/models.py and it has a view that "GET", "DELETE", "PUT", and "POST" in service/service_rest/views.py

        Appointment also has a details view in service/service_rest/views.py

## Sales microservice

The sales is made up of:

Customer:
    Customers it is independent from any info in inventory.

    Customer has a model in sales_rest/models.py and it has a view that "GET" and "POST" in sales/sales_rest/views.py


Salesperson:
    Salesperson it is independent from any info in inventory.

    Salesperson has a model in sales_rest/models.py and it has a view that "GET" and "POST" in sales/sales_rest/views.py


Sales:
    Sales is dependent on the information from inventory specifically automobiles' vin and sales' customer and salesperson.

    Salesperson has a model in sales_rest/models.py and it has a view that "GET" and "POST" in sales/sales_rest/views.py

    SalesList: list of sales
    SalespersonHistory: unique to sales it filters sales based on the salesperson choosen in the dropdown, it is a way to filter sales that the salesperson.
    SalesForm: This feature is currently broken. All drop downs work and the only error given is when the form is submit. Currently admin is the only way to add a sale. Instructions in How to Run this app.

## How to Run this App
Verify Docker, Git, Node.js and Insomnia is installed

1. Fork this repository

2. Clone the forked responsitory
    type this in terminal: git clone "replace with cloned respository here"

3. Build and run project in Docker:
    docker volume create beta-data
    docker-compose build
    docker-compose up

    verify docker containers are running

    view project at http://localhost:3000/

4. Create manufacturer, vehiclemodel, and automobile. This info is required for Service's appointment and Sale's sale.

## Diagram

 ![diagram of carcar](images/diagram.png)

## API Documentation

### URLs and Ports
Inventory:
    List manufacturers	GET	http://localhost:8100/api/manufacturers/
    Create a manufacturer	POST	http://localhost:8100/api/manufacturers/
    Get a specific manufacturer	GET	http://localhost:8100/api/manufacturers/:id/
    Update a specific manufacturer	PUT	http://localhost:8100/api/manufacturers/:id/
    Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/:id/

    manufacturer:
        create: POST
            {
            "name": "Chrysler"
            }
        read: GET http://localhost:8100/api/manufacturers/

        update: PUT
            {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "Dodge"
            }
        delete: DELETE http://localhost:8100/api/manufacturers/

    List vehicle models	GET	http://localhost:8100/api/models/
    Create a vehicle model	POST	http://localhost:8100/api/models/
    Get a specific vehicle model	GET	http://localhost:8100/api/models/:id/
    Update a specific vehicle model	PUT	http://localhost:8100/api/models/:id/
    Delete a specific vehicle model	DELETE	http://localhost:8100/api/models/:id/

    vehicle models:
        create: POST
            {
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
            "manufacturer_id": 1
            }

        read: GET http://localhost:8100/api/models/:id/

        update: PUT
            {
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
            }

        delete: DELETE http://localhost:8100/api/models/:id/

    List automobiles	GET	http://localhost:8100/api/automobiles/
    Create an automobile	POST	http://localhost:8100/api/automobiles/
    Get a specific automobile	GET	http://localhost:8100/api/automobiles/:vin/
    Update a specific automobile	PUT	http://localhost:8100/api/automobiles/:vin/
    Delete a specific automobile	DELETE	http://localhost:8100/api/automobiles/:vin/

    automobiles:
        create: POST
            {
            "color": "red",
            "year": 2012,
            "vin": "1C3CC5FB2AN120174",
            "model_id": 1
            }

        read: GET http://localhost:8100/api/automobiles/
            {
            "href": "/api/automobiles/1C3CC5FB2AN120174/",
            "id": 1,
            "color": "yellow",
            "year": 2013,
            "vin": "1C3CC5FB2AN120174",
            "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/1/",
                "id": 1,
                "name": "Daimler-Chrysler"
            }
            },
            "sold": false
            }

        update: PUT
            {
            "color": "red",
            "year": 2012,
            "sold": true
            }

        delete: DELETE http://localhost:8100/api/automobiles/:vin/

Sales:
    List salespeople	GET	http://localhost:8090/api/salespeople/
    Create a salesperson	POST	http://localhost:8090/api/salespeople/
    List customers	GET	http://localhost:8090/api/customers/
    Create a customer	POST	http://localhost:8090/api/customers/
    List sales	GET	http://localhost:8090/api/sales/
    Create a sale	POST	http://localhost:8090/api/sales/

    Full functionality is availible for all sales services on CarCar except Sales creation.

    JSON body to send data using insomnia:

    create salesperson:
        {
	"first_name": "firstname",
	"last_name": "lastname",
	"employee_id": "number"
    }

    create customer:
        {
	"first_name": "firstname",
	"last_name": "lastname",
	"address": "address",
	"phone_number": "number no more than 17characters"
        }

    create sale:
        NOTE create sale is currently not operational in insomnia or CarCar. Current creation availble on django admin.

        1. Open docker container sales-api-1
        2. Open Exec tab
        3. Enter: python manage.py createsuperuser
            fill out prompted user data
        4. Go to: http://localhost:8090/admin
        5. Assuming an automobile has already been made and added to database
        click on +ADD for Sales.
            then fill out form and save.



Services:
    List technicians	GET	http://localhost:8080/api/technicians/
    Create a technician	POST	http://localhost:8080/api/technicians/
    Delete a specific technician	DELETE	http://localhost:8080/api/technicians:id/
    List appointments	GET	http://localhost:8080/api/appointments/
    Create an appointment	POST	http://localhost:8080/api/appointments/
    Delete an appointment	DELETE	http://localhost:8080/api/appointments/:id/
    Set appointment status to "canceled"	PUT	http://localhost:8080/api/appointments/:id/cancel/
    Set appointment status to "finished"	PUT	http://localhost:8080/api/appointments/:id/finish/

    create technician:

    create appointment:

### Inventory API (Optional)
 - Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.

### Service API
Technician:
    TechnicianList: list of technicians
    TechnicianForm: creation form for technicians

Appointment:
    AppointmentList: list of appointments
    AppointmentForm: creation form for appointments
    ServiceHistory: list of services based on the vin of the automobile.

### Sales API
Customer:
    CustomerList: list of customer
    CustomerForm: creation form for customers

Salesperson:
    SalespersonList: list of salespeople
    SalespersonForm: creation form for salesperson

Sales:
    SalesList: list of sales
    SalespersonHistory: unique to sales it filters sales based on the salesperson choosen in the dropdown, it is a way to filter sales that the salesperson.
    SalesForm: This feature is currently broken. All drop downs work and the only error given is when the form is submit. Currently admin is the only way to add a sale. Instructions in How to Run this app.

## Value Objects
 Service:
    AutomobileVO: contains vin and sold field from automobile.
 Sales
    AutomobileVO: contains vin and sold field from automobile.
